
function copyToClipboard(text, callback) {
    if (window.clipboardData && window.clipboardData.setData) {
        // Internet Explorer-specific code path to prevent textarea being shown while dialog is visible.
        clipboardData.setData("Text", text);
        callback();

    } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
        var textarea = document.createElement("input");
        textarea.value = text;
        textarea.style.position = "fixed"; // Prevent scrolling to bottom of page in Microsoft Edge.
        document.body.appendChild(textarea);
        textarea.select();
        textarea.setSelectionRange(0, 99999); /*For mobile devices*/
        try {
            document.execCommand("copy"); // Security exception may be thrown by some browsers.
            callback();
        } catch (ex) {
            console.warn("Copy to clipboard failed.", ex);
            return false;
        } finally {
            document.body.removeChild(textarea);
        }
    }
}

pass = get_password('dffdfdf')
console.log('pass', pass)

let update_pass = () => {
    let domain = document.getElementById('domain').value.toLowerCase().replace(/\s/g, '');
    let user_pass = document.getElementById('your_password').value;
    let output_password = get_password(user_pass, 12, '#@', domain);
    document.getElementById('output_password').value = output_password;
}

document.getElementById('domain').onkeyup = update_pass
document.getElementById('domain').onchange = update_pass

document.getElementById('your_password').onkeyup = update_pass
document.getElementById('your_password').onchange = update_pass

document.getElementById('show_pass1').onclick = () => {
    let text = document.getElementById('show_pass1').innerText;
    if (text == 'Show') {
        document.getElementById('your_password').type = 'text';
        document.getElementById('show_pass1').innerText = 'Hide';
    } else {
        document.getElementById('your_password').type = 'password';
        document.getElementById('show_pass1').innerText = 'Show';
    }
}


document.getElementById('show_pass2').onclick = () => {
    let text = document.getElementById('show_pass2').innerText;
    if (text == 'Show') {
        document.getElementById('output_password').type = 'text';
        document.getElementById('show_pass2').innerText = 'Hide';
    } else {
        document.getElementById('output_password').type = 'password';
        document.getElementById('show_pass2').innerText = 'Show';
    }
}


document.getElementById('copy_pass').onclick = () => {
    let pass = document.getElementById('output_password').value;
    copyToClipboard(pass, () => {
        document.getElementById('copy_pass').innerText = 'Copied!';
        console.log('copied');
        setTimeout(() => {
            document.getElementById('copy_pass').innerText = 'Copy';
        }, 1500)
    })
    // add to all password fields 

    // var inputs = document.getElementsByTagName("input");
    // for (var i = 0; i < inputs.length; i++) {
    //     if (inputs[i].type.toLowerCase() === "password") {
    //         inputs[i].value = pass;
    //     }
    // }
    add_pass_to_fuilds(pass);

}


$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});