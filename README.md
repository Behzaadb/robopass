## Features

- Converts your password into a hard-to-guess phrase.
- Makes your passwords unique for each website; no common password.
- Meets password requirements of most websites by deploying the strongest passwords.
- Leaves no trace of your password anywhere.
- Open Source

![](https://i.ibb.co/71xtNvV/logo2.png)


RoboPass takes your password in forms, and converts them to strong passwords with combination of hard-to-guess letters and numbers. This extension does not store any password anywhere, it's just a converter.

People usually pick the same password for all their accounts. RoboPass solves this by using the domain name to generate the strong password; therefore, the password would be always domain-specific. It means if your password gets revealed on one website, it will not be possible to use it on any other site; and you still can use the same password for all your accounts.

RoboPass makes sure final password always contains upper case letters, lower cases, digits, and special characters. Since converted password is hash-generated, it is not possible to use common dictionary brute force hack attacks on your password. It also uses slow hashing to make sure mass try-and-error attacks to get your password be hard to be done.

Robopass never stores your password anywhere. Remembered passwords on your browser could be stolen, so the safest place to keep your passwords is in your own memory! RoboPass does the job of changing easy-to-remember phrases into complicated passwords for you :)


## To Install

- FireFox: [https://addons.mozilla.org/en-US/firefox/addon/robopass/](https://addons.mozilla.org/en-US/firefox/addon/robopass/)