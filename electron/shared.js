function hexToBase64(s, sp_ch) {
    var defaultTableStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    if (sp_ch == undefined)
        sp_ch = '#@'
    let tableStr = defaultTableStr + sp_ch;
    while (s.length % 3 != 0) {
        s = '0' + s;
    }
    out = ''
    for (let i = s.length - 1; i >= 2; i = i - 3) {
        let c1 = s[i];
        let c2 = s[i - 1];
        let c3 = s[i - 2];
        let n = parseInt(c1, 16) + parseInt(c2, 16) * 16 + parseInt(c3, 16) * 256
        let enc2 = tableStr.charAt(Math.floor(n / 64))
        let enc1 = tableStr.charAt(n % 64)
        out = out + enc2 + enc1
    }
    return out
}


var get_password = function (password, max_len, allowed_special_characters, user_domain) {
    // This function generates a hash-like password 
    if (password === undefined)
        password = 'mypassword';
    if (max_len === undefined)
        max_len = 12;
    if (allowed_special_characters === undefined)
        allowed_special_characters = "@#!";

    if (user_domain === undefined)
        user_domain = window.location.hostname.split('.').slice(-2).join('.');

    user_domain = user_domain.trim().toLowerCase()

    if (password.trim().length < 6)
        return ''

    var t0 = performance.now()


    str_to_encr = 'someSalt' + user_domain + 'someMoreSalt' + password
    var passhash = CryptoJS.MD5(str_to_encr).toString();


    // added a time consuming procedure here
    // loop 100 MD5
    let start_number = 1000 + parseInt(passhash.slice(-3), 16);
    for (let i = 0; i < 100; i++) {
        let n = Math.round(Math.pow((i + start_number), 2.13)).toString();

        let salt = 'S1' + n.slice(1, 3) + 'sOmeConsTanTSaLt' + n.slice(-1);

        let str_base64 = hexToBase64(passhash, allowed_special_characters)
        let new_str = salt + str_base64;
        passhash = CryptoJS.MD5(new_str).toString();
    }

    let str_base64 = hexToBase64(passhash, allowed_special_characters)


    new_pass = str_base64.slice(0, max_len)

    // check if has a all required characters
    let characters_arr = [allowed_special_characters, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz", "0123456789"];
    characters_arr.forEach((characters) => {

        let contains_sp_ch = false;
        for (var i = 0; i < characters.length; i++) {
            let s = characters.charAt(i);
            if (new_pass.indexOf(s) > -1) {
                contains_sp_ch = true;
                break
            }
        }
        if (contains_sp_ch === false && characters.length > 0) {
            let determiner_number = parseInt(passhash.slice(0, 3), 16);
            let n = determiner_number % max_len;
            let c = characters[determiner_number % characters.length]
            new_pass = new_pass.substring(0, n) + c + new_pass.substring(n + 1);
        }

    })



    var t1 = performance.now()
    // console.log("new_pass took " + (t1 - t0) + " milliseconds.")

    return new_pass

}