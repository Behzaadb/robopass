Passume 
Copyright (c) 30 Denver Artifial Intelligence LLC


*** END USER LICENSE AGREEMENT ***  



IMPORTANT: PLEASE READ THIS LICENSE CAREFULLY BEFORE USING THIS SOFTWARE.


1. LICENSE


By receiving, opening the file package, and/or using Passume ("Software") containing this software, you agree that this End User User License Agreement(EULA) is a legally binding and valid contract and agree to be bound by it. You agree to abide by the intellectual property laws and all of the terms and conditions of this Agreement.



Unless you have a different license agreement signed by Denver Artifial Intelligence LLC your use of Passume  indicates your acceptance of this license agreement and warranty.



Subject to the terms of this Agreement, Denver Artifial Intelligence LLC grants to you a limited, non-exclusive, non-transferable license, without right to sub-license, to use Passume  in accordance with this Agreement and any other written agreement with Denver Artifial Intelligence LLC. Denver Artifial Intelligence LLC does not transfer the title of Passume  to you; the license granted to you is not a sale. This agreement is a binding legal agreement between Denver Artifial Intelligence LLC and the purchasers or users of Passume .



If you do not agree to be bound by this agreement, remove Passume  from your computer now and, if applicable, promptly return to Denver Artifial Intelligence LLC by mail any copies of Passume  and related documentation and packaging in your possession. 

	
2. DISTRIBUTION


Passume  and the license herein granted shall not be copied, shared, distributed, re-sold, offered for re-sale, transferred or sub-licensed in whole or in part except that you may make one copy for archive purposes only. For information about redistribution of Passume  contact Denver Artifial Intelligence LLC.

	
3. USER AGREEMENT

3.1 Use


Your license to use Passume  is limited to the number of licenses purchased by you. You shall not allow others to use, copy or evaluate copies of Passume .


3.2 Use Restrictions


You shall use Passume  in compliance with all applicable laws and not for any unlawful purpose. Without limiting the foregoing, use, display or distribution of Passume  together with material that is pornographic, racist, vulgar, obscene, defamatory, libelous, abusive, promoting hatred, discriminating or displaying prejudice based on religion, ethnic heritage, race, sexual orientation or age is strictly prohibited.



Each licensed copy of Passume  may be used on one single computer location by one user. Use of Passume  means that you have loaded, installed, or run Passume  on a computer or similar device. If you install Passume  onto a multi-user platform, server or network, each and every individual user of Passume  must be licensed separately.



You may make one copy of Passume  for backup purposes, providing you only have one copy installed on one computer being used by one person. Other users may not use your copy of Passume  . The assignment, sublicense, networking, sale, or distribution of copies of Passume  are strictly forbidden without the prior written consent of Denver Artifial Intelligence LLC. It is a violation of this agreement to assign, sell, share, loan, rent, lease, borrow, network or transfer the use of Passume . If any person other than yourself uses Passume  registered in your name, regardless of whether it is at the same time or different times, then this agreement is being violated and you are responsible for that violation!  


3.3 Copyright Restriction


This Software contains copyrighted material, trade secrets and other proprietary material. You shall not, and shall not attempt to, modify, reverse engineer, disassemble or decompile Passume . Nor can you create any derivative works or other works that are based upon or derived from Passume  in whole or in part.

	

Denver Artifial Intelligence LLC's name, logo and graphics file that represents Passume  shall not be used in any way to promote products developed with Passume  . Denver Artifial Intelligence LLC retains sole and exclusive ownership of all right, title and interest in and to Passume  and all Intellectual Property rights relating thereto.



Copyright law and international copyright treaty provisions protect all parts of Passume , products and services. No program, code, part, image, audio sample, or text may be copied or used in any way by the user except as intended within the bounds of the single user program. All rights not expressly granted hereunder are reserved for Denver Artifial Intelligence LLC. 


3.4 Limitation of Responsibility


You will indemnify, hold harmless, and defend Denver Artifial Intelligence LLC , its employees, agents and distributors against any and all claims, proceedings, demand and costs resulting from or in any way connected with your use of Denver Artifial Intelligence LLC's Software.



In no event (including, without limitation, in the event of negligence) will Denver Artifial Intelligence LLC , its employees, agents or distributors be liable for any consequential, incidental, indirect, special or punitive damages whatsoever (including, without limitation, damages for loss of profits, loss of use, business interruption, loss of information or data, or pecuniary loss), in connection with or arising out of or related to this Agreement, Passume  or the use or inability to use Passume  or the furnishing, performance or use of any other matters hereunder whether based upon contract, tort or any other theory including negligence.


 
Denver Artifial Intelligence LLC's entire liability, without exception, is limited to the customers' reimbursement of the purchase price of the Software (maximum being the lesser of the amount paid by you and the suggested retail price as listed by Denver Artifial Intelligence LLC ) in exchange for the return of the product, all copies, registration papers and manuals, and all materials that constitute a transfer of license from the customer back to Denver Artifial Intelligence LLC.

	
3.5 Warranties


Except as expressly stated in writing, Denver Artifial Intelligence LLC makes no representation or warranties in respect of this Software and expressly excludes all other warranties, expressed or implied, oral or written, including, without limitation, any implied warranties of merchantable quality or fitness for a particular purpose.


3.6 Governing Law


This Agreement shall be governed by the law of the US applicable therein. You hereby irrevocably attorn and submit to the non-exclusive jurisdiction of the courts of US therefrom. If any provision shall be considered unlawful, void or otherwise unenforceable, then that provision shall be deemed severable from this License and not affect the validity and enforceability of any other provisions.


3.7 Termination


Any failure to comply with the terms and conditions of this Agreement will result in automatic and immediate termination of this license. Upon termination of this license granted herein for any reason, you agree to immediately cease use of Passume  and destroy all copies of Passume  supplied under this Agreement. The financial obligations incurred by you shall survive the expiration or termination of this license.  


4. DISCLAIMER OF WARRANTY


THIS SOFTWARE AND THE ACCOMPANYING FILES ARE SOLD "AS IS" AND WITHOUT WARRANTIES AS TO PERFORMANCE OR MERCHANTABILITY OR ANY OTHER WARRANTIES WHETHER EXPRESSED OR IMPLIED. THIS DISCLAIMER CONCERNS ALL FILES GENERATED AND EDITED BY Passume  AS WELL.


5. CONSENT OF USE OF DATA


You agree that Denver Artifial Intelligence LLC may collect and use information gathered in any manner as part of the product support services provided to you, if any, related to Passume .Denver Artifial Intelligence LLC may also use this information to provide notices to you which may be of use or interest to you.
 	
	