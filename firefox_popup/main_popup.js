console.log('Robopass popup page');

var isChrome = !!window.chrome;
try {
    browser = browser;
} catch (error) {
    browser = window.chrome;
}

function storage_get_all(callback) {
    if (isChrome) {
        browser.storage.local.get(null, callback);
    }
    else {
        browser.storage.local.get().then(callback);
    }
}

var url = '';
var domain = '';
var enabled = true;
var disable_btn = document.getElementById("disable_me")
disable_btn.innerText = "Enabled on this domain";


let tabs_callback = (tabs) => {
    url = tabs[0].url
    domain = (new URL(url)).hostname;
    domain = domain.split('.').slice(-2).join('.');
    // console.log('main popup domain', domain)
    let key = "disabled_" + domain;
    let browser_callback = (out) => {
        if ((key in out)) {
            enabled = false;
            disable_btn.innerText = "Disabled on this domain";
        }

    }
    storage_get_all(browser_callback);

    browser_callback = (out) => {
        let allowed_chars = '#@'
        let k = 'allowed_chars_' + domain
        if (k in out) {
            allowed_chars = out[k];
        }
        '#@'.split('').forEach((c) => {
            if (allowed_chars.includes(c) === true)
                document.getElementById('ch' + c).checked = true;
            else
                document.getElementById('ch' + c).checked = false;
        })

    }
    storage_get_all(browser_callback);
}
if (isChrome)
    browser.tabs.query({ currentWindow: true, active: true }, tabs_callback);
else
    browser.tabs.query({ currentWindow: true, active: true }).then(tabs_callback);

var disable_me = () => {
    // This function disables or enables the app 
    let key = "disabled_" + domain
    let data = {}
    data[key] = true

    if (enabled === true) {

        let storage_callback = () => {
            // console.log('disabled');
            disable_btn.innerText = "Disabled on this domain";
            enabled = false;
        };
        if (isChrome)
            browser.storage.local.set(data, storage_callback);
        else
            browser.storage.local.set(data).then(storage_callback);
    } else {

        // enable again 
        let storage_callback = () => {
            // console.log('enabled');
            disable_btn.innerText = "Enabled on this domain";
            enabled = true;
        };
        if (isChrome)
            browser.storage.local.remove(key, storage_callback);
        else
            browser.storage.local.remove(key).then(storage_callback);
    }

}




var update_allowed_chars = function () {
    let allowed_chars = ''
    '#@'.split('').forEach((c) => {
        if (document.getElementById('ch' + c).checked === true)
            allowed_chars = allowed_chars + c
    })
    let key = 'allowed_chars_' + domain
    let data = {}
    data[key] = allowed_chars
    browser.storage.local.set(data)
}


'#@'.split('').forEach((c) => {
    document.getElementById('ch' + c).onchange = update_allowed_chars;
})


document.getElementById('disable_me').onclick = disable_me


