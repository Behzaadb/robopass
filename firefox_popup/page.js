// doc: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions 

let domain = window.location.hostname
domain = domain.split('.').slice(-2).join('.');
console.log('domain:', domain)

let allowed_chars = '#@'
let robopass_enabled = true;

// console.log('Robopass Started')
var isChrome = !!window.chrome;
try {
    browser = browser;
} catch (error) {
    browser = window.chrome;
}

function storage_get_all(callback) {
    if (isChrome) {
        browser.storage.local.get(null, callback);
    } else {
        browser.storage.local.get().then(callback);
    }
}




function base64ToHex(s, sp_ch) {
    var defaultTableStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    if (sp_ch == undefined)
        sp_ch = '#@'
    let tableStr = defaultTableStr + sp_ch;
    out = ''
    for (let i = s.length - 1; i >= 1; i = i - 2) {
        let enc1 = s[i];
        let enc2 = s[i - 1];
        let n = tableStr.indexOf(enc1) + tableStr.indexOf(enc2) * 64
        let c = n.toString(16)
        out = out + c
    }
    return out
}


var cumulativeOffset = function (element) {
    // This function finds the overal left and top offset of the element 
    var top = 0,
        left = 0;
    do {
        top += element.offsetTop || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while (element);

    return {
        top: top,
        left: left
    };
};




function getPwdInputs() {
    // This function find all password fields on the page 
    let ary = [];
    let inputs = document.getElementsByTagName("input");
    for (let i = 0; i < inputs.length; i++) {
        let el = inputs[i]
        let t = el.getAttribute('type')
        if (t !== null && t.toLowerCase() === "password") {
            ary.push(el);
        }
    }
    return ary;
}





var type_password = function (el) {
    // This function pretends that the hash password is being typed, so 
    // websites can run their pass-check 

    var evt = document.createEvent("MouseEvents");
    evt.initMouseEvent("click", true, true, window,
        0, 0, 0, 0, 0, false, false, false, false, 0, null);
    el.dispatchEvent(evt);

    let event_arr = ['change', 'keydown', 'keyup'];
    event_arr.forEach((ev_type) => {

        let ev = document.createEvent('HTMLEvents');
        ev.initEvent(ev_type, true, true);
        el.dispatchEvent(ev);
    })

}


var dont_close_box = false;
var generate_input_box = function (input_pass_el, idx) {
    // This function generates a box below actual password fields of the website
    var fullURL = browser.runtime.getURL("firefox_popup/page.html");
    console.log('url', fullURL);

    var myDiv = document.createElement("div");

    console.log("injecting pass container")
    myDiv.innerHTML += `<iframe rel="import" src="` + fullURL + `">`;

    //Finally, append the element to the HTML body
    document.body.appendChild(myDiv);
    // var ajax = new XMLHttpRequest();
    // ajax.open("GET", fullUrl, false);
    // ajax.send(null);
    // // input_pass_el.innerHTML += ajax.responseText;
    // console.log("finished inj", ajax)
    fetch(fullUrl).then(data => console.log(data));

    //

    var font_url = browser.runtime.getURL("fonts/Lato-Bold.ttf");
    var css = `
        @font-face {
            font-family: RoboPass;
            src: url(${font_url});
        }
    `;
    head = document.head || document.getElementsByTagName('head')[0];
    style = document.createElement('style');
    head.appendChild(style);
    style.type = 'text/css';
    style.appendChild(document.createTextNode(css));



    input_pass_el.style.backgroundColor = '#f7ff00';
    input_pass_el.style['-webkit-transition'] = 'background-color 500ms cubic-bezier(0.65, 0.05, 0.36, 1) 0s';

    const input_pass_el_style = cumulativeOffset(input_pass_el); //input_pass_el.getBoundingClientRect()// getComputedStyle(input_pass_el)


    let input_pass_el_width = input_pass_el.offsetWidth;
    let input_div_top = input_pass_el_style.top + input_pass_el.offsetHeight + 3;
    let input_div_left = input_pass_el_style.left;
    let input_div_width = input_pass_el_width - 5;
    input_pass_el.value = '';
    let overflow_mode = input_pass_el.parentNode.style.overflow;

    let input_wrapper = document.createElement('DIV');
    input_wrapper.setAttribute("id", "passume_container")
    input_wrapper.setAttribute(
        "style", `
            all: initial;
            display: fixed; 
            `
    )

    let input_div = document.createElement('DIV');

    input_wrapper.appendChild(input_div)


    input_div.class = "passGuyDiv";

    input_div.setAttribute(
        "style", `
            -ms-flex-pack: center;
            -webkit-box-pack: center;
            justify-content: center;
            color: #fff;
            text-shadow: 0 .05rem .1rem rgba(0, 0, 0, .5);
            box-shadow: inset 0 0 5rem rgba(0, 0, 0, .5);
            background-color: rgb(86, 7, 121);
            margin: 2px 5px;
            padding: 8px; 
            position: absolute;
            border: 1px solid #BFBFBF;
            box-shadow: 5px 5px 5px #aaaaaa;
            z-index: 100000;
            box-sizing: content-box;
            opacity:1;
            color: white;

            font-weight: normal;
            font-size: 16px;
            line-height: normal;
            font-family: "RoboPass", sans-serif, Arial;
            direction: ltr;
            white-space: normal;

            transition: none;
            -webkit-transition: none; 

            top: 5px;
            right: 10px;

            display: none
            `);

    // top: ${input_div_top}px;
    // left: ${input_div_left}px;

    $("#yourDiv").load('readHtmlFromHere.html #readMe');



    let input_el = document.createElement('INPUT');
    input_el.setAttribute("class", "passGuyInput");
    input_el.setAttribute("type", "password");
    let input_el_width = input_div_width - 10;
    input_el.setAttribute(
        "style", `
            min-width: ${input_el_width}px;
            height: 40px;
            background-color: white;
            color: black;
            padding: 5px;

            font-weight: normal;
            font-size: 28px;
            line-height: normal;
            font-family: "RoboPass", Arial, sans-serif;

            transition: none;
            -webkit-transition: none;
            border-style: none;
            `);



    // added onchange to trigger new password generation 
    let new_pass_trigger = () => {
        let val = input_el.value
        if (val.trim().length < 6) {
            document.getElementById('passGuy_check_img' + idx).style.display = 'none'
            document.getElementById('passGuy_wrong_img' + idx).style.display = 'unset'
            input_pass_el.style.backgroundColor = '#f7ff00';
        } else {
            document.getElementById('passGuy_check_img' + idx).style.display = 'unset'
            document.getElementById('passGuy_wrong_img' + idx).style.display = 'none'
            input_pass_el.style.backgroundColor = 'rgb(80, 255, 0)';
        }
        new_pass = get_password(val, 12, allowed_chars)
        input_pass_el.value = new_pass
        type_password(input_pass_el)
    }

    input_el.onkeyup = new_pass_trigger
    input_el.onchange = new_pass_trigger

    // as soon as user focuses on a password field on the website, the box would appear to get the pass and convert it to hash-like password
    input_pass_el.onfocus = () => {
        dont_close_box = true;
        setTimeout(() => { dont_close_box = false }, 500)
        input_div.style.display = 'block';
        input_pass_el.parentNode.style.overflow = 'visible';
        input_el.focus();
    }

    // on moving away the box disappears 
    input_el.onblur = () => {
        setTimeout(() => {

            if (dont_close_box === false) {
                input_pass_el.parentNode.style.overflow = overflow_mode;
                input_div.style.display = 'none';
                input_pass_el.type = 'password';
                input_el.type = 'password';
                input_show.innerText = 'Show';
            }

        }, 300)
    }


    let input_el_wrapper = document.createElement('SPAN');

    input_div.appendChild(input_el_wrapper)
    input_el_wrapper.appendChild(input_el)

    let input_show = document.createElement('BUTTON');
    input_show.type = 'button';
    input_show.innerText = 'Show';
    let botton_top = input_el.offsetHeight / 2;
    input_show.setAttribute(
        "style", `
            color: rgb(38,38,38);
            border: 0;
            color: #0095f6;
            color: rgba(var(--d69,0,149,246),1);
            display: inline;
            padding: 10px;
            position: absolute;
            background-color: transparent;
            background: 0 0;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            cursor: pointer;
            font-weight: 600;
            text-align: center;
            text-transform: inherit;
            text-overflow: ellipsis;
            top: 10px; 
            right:6px; 
            `);

    // This is a show/hide mechanism 
    input_show.onclick = function () {
        let v = input_show.innerText;
        if (v === 'Show') {
            input_show.innerText = 'Hide';
            input_pass_el.type = 'text';

            input_el.style.fontSize = '18px';
            input_el.type = 'text'
        } else {
            input_show.innerText = 'Show'
            input_pass_el.type = 'password'

            input_el.style.fontSize = '28px'
            input_el.type = 'password'
        }
        dont_close_box = true;
        setTimeout(() => { dont_close_box = false }, 500)
        input_el.focus()
    }


    input_el_wrapper.appendChild(input_show)

    let check_len = document.createElement('DIV');
    check_len.setAttribute(
        "style", ` 
        display: block;
        margin: 3px 0 7px 0;
        `
    )
    check_len.innerHTML = `<span style="display: flex;"><img id="passGuy_check_img${idx}" src="${check_img}" style="height:16px; width:16px; margin:0 5px;display:none">
                                 <img id="passGuy_wrong_img${idx}" src="${wrong_img}"  style="height:16px;width:16px; margin:0 5px;">
                                 <b>Use 6 or more characters.</b></span>`

    input_div.appendChild(check_len)

    let logo = document.createElement('DIV');
    var logo_url = browser.runtime.getURL("icons/i16.ico");
    logo.innerHTML = `<span><img src="${logo_url}" style="height:16px;width:16px">&nbsp;RoboPass</span>`
    logo.setAttribute(
        "style", `
            font-weight: 900;
            float: right; 
            display: flex; 
            `);

    input_div.appendChild(logo)



    input_div.style.display = 'none';
    return input_wrapper

}



var input_obj = {}

function ini_robopass() {
    if (robopass_enabled == false) {
        return
    }

    input_password_arr = getPwdInputs();


    for (let i = 0; i < input_password_arr.length; i++) {
        input_pass_el = input_password_arr[i];

        if (input_pass_el.hasAttribute('id') === false)
            input_pass_el.id = 'passGuy_input_pass_el' + Math.floor(Math.random() * 10000000);

        if ((input_pass_el.id in input_obj) || (input_pass_el.getAttribute('class').includes("passGuyInput") === true) || getComputedStyle(input_pass_el, null).display === 'none') {

            continue
        }



        let input_box = generate_input_box(input_pass_el, i)
        document.getElementsByTagName("BODY")[0].appendChild(input_box) // input_pass_el.parentNode.appendChild(input_box) //attach to its parent 

        input_obj[input_pass_el.id] = input_box
    }

    if (robopass_enabled == true) {
        setTimeout(() => { ini_robopass() }, 3000); // inifit loop for SPA websites 
    }



}

function add_pass_to_fuilds(p) {
    let input_password_arr = getPwdInputs();


    for (let i = 0; i < input_password_arr.length; i++) {
        input_pass_el = input_password_arr[i];
        input_pass_el.value = p;
    }

}



function run() {
    let storage_callback = (out) => {
        let key = 'allowed_chars_' + domain;
        if (key in out) {
            allowed_chars = out[key];
        }

        key = "disabled_" + domain;
        if (!(key in out)) {
            robopass_enabled = true;
            setTimeout(ini_robopass, 3000);
        } else {
            robopass_enabled = false;
            disable_here();
        }

    }
    storage_get_all(storage_callback);
}

function disable_here() {
    Object.keys(input_obj).forEach((input_pass_el_id) => {
        input_pass_el = document.getElementById(input_pass_el_id);
        input_pass_el.style.backgroundColor = 'unset';
        input_pass_el.type = 'password';
        input_pass_el.value = '';
        input_pass_el.onfocus = () => { return null }
        input_box = input_obj[input_pass_el_id]
        input_box.parentNode.removeChild(input_box);
    })
    input_obj = {}
}


if (domain.includes(".") && domain != "robopass.me") {
    browser.storage.onChanged.addListener(run);
    run();
}
console.log("rpass is loaded")